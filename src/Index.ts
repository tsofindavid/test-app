import app from "./app"
import * as config from "../config.json"

async function main() {
  (await app).listen(config.APP_PORT)
  console.log(`Server is listening on port: ${config.APP_PORT}`)
}
main()