class Middleware {
  static async simpleMiddleware(ctx, next) {
    console.log('simpleMiddleware')
    ctx.data = { simpleMiddleware: { status: 'ok', data: await ctx.users.find().toArray() } }
    await next()
  }
}

export default Middleware