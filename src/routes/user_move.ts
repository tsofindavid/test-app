import {Models} from "../models/index"

class User {
    static async createRandomUser(num) {
        let id = {}
        let type = "all"
        
        let users = await Models.getUsers(id, type)
        
        console.log(num)
        for (let i = 1; i < num + 1; i++){
            let user = {
                id :  users.length + i,
                name : "user",
                cash : Math.floor(Math.random() * Math.floor(100000))
            }
            Models.writeData(user)
            console.log(user)
        }
        return "completed"
    }

    static async createManualUser(data){
        let {id, name , cash} = data
        let users = await Models.getUsers(id, "all")

        
    
        let user = {
            id :  users.length + 1,
            name : name,
            cash : cash
        }
        Models.writeData(user)
        return "completed"

    }

    static async move_cash(move, id, sum) {
        
        let user_data = await Models.getUsers(id, "search")
        
        if (move === "get"){
        user_data[0].cash = user_data[0].cash - sum
        }
        else if(move === "put"){
            user_data[0].cash = user_data[0].cash + sum
        }
        console.log(user_data)
        Models.reload_data(user_data)
        return [user_data, "completed"]

    }
    
    static async transferCash(id_1, id_2, sum){
        let user_1 = await Models.getUsers(id_1, "search")
        let user_2 = await Models.getUsers(id_2, "search")
        console.log(user_1, user_2)
        if (user_1.length > 0 && user_2.length >0){
            console.log("transfer")
            user_1[0].cash = user_1[0].cash - sum
            user_2[0].cash = user_2[0].cash + sum
            Models.reload_data(user_1)
            Models.reload_data(user_2)

        }else{
            console.log("error")
            return("error")
        }
        return ["completed",user_1, user_2]
        
    }
}

export {User}