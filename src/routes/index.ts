import * as router from "koa-router"
import {Models} from "../models/index"
import {User} from "./user_move"

const route = new router()



route
  .get("/list", async (ctx, next)=>{
    let id = {}
    let type = "all"
    let users = await Models.getUsers(id, type)
    ctx.body = {
      status:"ok",
      users: users
    }
    console.log(users)
    await next()
  })
  
  .post("/add_user", async (ctx, next)=>{
    let {type, num} = ctx.body
    
    if (type === "random"){
      
      ctx.body = await User.createRandomUser(num)
      
    }else if(type === "manual"){
      ctx.body = await User.createManualUser(ctx.body)
    }
    
    await next()
  })

  .post("/move_cash", async (ctx, next)=>{
    let {move, id , sum} = ctx.body
    ctx.body = await User.move_cash(move, id, sum)
    await next()
  })

  .post("/transfer_cash", async (ctx , next)=>{
    console.log("trans")
    let {id_1, id_2, sum} = ctx.body
    ctx.body = await User.transferCash(id_1, id_2, sum)
    
    await next()
  })


route.post('/list',(ctx)=>{
  console.log(ctx.body)
  console.log(ctx.request.body)
  console.log("xui")
  ctx.body = {
    status: 'ok'
  }
})

export default route.middleware();