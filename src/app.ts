import * as koa from 'koa'
import * as router from './routes'
import * as bodyParser from "koa-bodyparser"


async function app() {
  const app = new koa()
  app.use(bodyParser())
  app.use(async (ctx, next)=> {
    // the parsed body will store in ctx.request.body
    // if nothing was parsed, body will be an empty object {}
    ctx.body = ctx.request.body;
    await next()
  });
  app.use(router.default)

  return app
}

export default app()