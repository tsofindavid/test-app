import * as mongodb from "mongodb";
import * as config from "../../config.json";




class Models {
    

    static async getUsers(id, type){
        let get
        const client = new mongodb.MongoClient(config.MONGODB_CONNECTION_STRING)
        const connection = await client.connect()
        const db = connection.db(config.MONGO_CONNECTION_NAME)
        if (type === "search"){
            get = await db.collection("test").find({id : id}).toArray()
            //console.log(type,", ", get)
            }
        else if (type === "all"){
            get = await db.collection("test").find().toArray()
            //console.log(type,", ", get)
        }
        client.close()
        
        return get;
    };

    static async writeData(data){
        const client = new mongodb.MongoClient(config.MONGODB_CONNECTION_STRING);  
        console.log(await data);  
        const connection = await client.connect();
        const db = connection.db(config.MONGO_CONNECTION_NAME);
        await db.collection("test").insertOne(await data);
        client.close();
        console.log(data, "uploaded");
    }

    static async reload_data(user_data){
        let {id, cash} = user_data[0]
        console.log(id, cash) 
        const client = new mongodb.MongoClient(config.MONGODB_CONNECTION_STRING);
        const connection = await client.connect();
        const db = connection.db(config.MONGO_CONNECTION_NAME);
        await db.collection("test").updateOne(
        {id : id }, {$set : {cash : cash}})
        //     // example
        // //   { name : "lol1" },
        // //   {
        // //     $set: {name:"lol1qqq"}
            
        // //   }
        // );
        client.close();
        
    }
    static async remove_data(id){
      

    }
}
    

  


export {Models}