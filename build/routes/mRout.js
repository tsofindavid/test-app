"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const router = require("koa-router");
const example_1 = require("./example");
const route = new router();
route.use(example_1.default);
route.get('/health', (ctx) => {
    ctx.body = {
        status: 'ok'
    };
});
exports.default = route.middleware();
//# sourceMappingURL=mRout.js.map