"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const router = require("koa-router");
const exampleRouter = new router({
    prefix: '/test'
});
exampleRouter.get('/lol', (ctx) => {
    ctx.body = "lol";
});
// exampleRouter.route('/kek')
//     .post((ctx)=>{
//   ctx.body = "kek"
// })
exampleRouter.get('/kek/:pth', (ctx) => {
    ctx.body = { pth: ctx.params.pth };
});
exports.default = exampleRouter.middleware();
//# sourceMappingURL=example.js.map