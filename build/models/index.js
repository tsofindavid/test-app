"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
class Models {
    constructor() {
        this.connection = null;
        this.createDBModelsInCTX = (ctx, next) => __awaiter(this, void 0, void 0, function* () {
            ctx.db = this.connection;
            ctx.users = this.connection.collection('users');
            yield next();
        });
    }
    connectToDB() {
        return __awaiter(this, void 0, void 0, function* () {
            this.connection = (yield mongodb_1.default.MongoClient("mongodb://localhost:27017/test").connect()).db();
        });
    }
}
exports.default = Models;
//# sourceMappingURL=index.js.map