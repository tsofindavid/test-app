"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Models = void 0;
const mongodb = require("mongodb");
const config = require("../../config.json");
class Models {
    static getUsers(id, type) {
        return __awaiter(this, void 0, void 0, function* () {
            let get;
            const client = new mongodb.MongoClient(config.MONGODB_CONNECTION_STRING);
            const connection = yield client.connect();
            const db = connection.db(config.MONGO_CONNECTION_NAME);
            if (type === "search") {
                get = yield db.collection("test").find({ id: id }).toArray();
                //console.log(type,", ", get)
            }
            else if (type === "all") {
                get = yield db.collection("test").find().toArray();
                //console.log(type,", ", get)
            }
            client.close();
            return get;
        });
    }
    ;
    static writeData(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = new mongodb.MongoClient(config.MONGODB_CONNECTION_STRING);
            console.log(yield data);
            const connection = yield client.connect();
            const db = connection.db(config.MONGO_CONNECTION_NAME);
            yield db.collection("test").insertOne(yield data);
            client.close();
            console.log(data, "uploaded");
        });
    }
    static reload_data(user_data) {
        return __awaiter(this, void 0, void 0, function* () {
            let { id, cash } = user_data[0];
            console.log(id, cash);
            const client = new mongodb.MongoClient(config.MONGODB_CONNECTION_STRING);
            const connection = yield client.connect();
            const db = connection.db(config.MONGO_CONNECTION_NAME);
            yield db.collection("test").updateOne({ id: id }, { $set: { cash: cash } });
            //     // example
            // //   { name : "lol1" },
            // //   {
            // //     $set: {name:"lol1qqq"}
            // //   }
            // );
            client.close();
        });
    }
    static remove_data(id) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.Models = Models;
//# sourceMappingURL=index.js.map