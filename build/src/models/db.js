"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const config = require("../../config.json");
mongoose.connect(config.MONGODB_CONNECTION_STRING, (err) => {
    if (err) {
        console.log(err.message);
    }
    else {
        console.log("Successfully Connected!");
    }
});
exports.default = connectToDB;
//# sourceMappingURL=db.js.map