"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const index_1 = require("../models/index");
class User {
    static createRandomUser(num) {
        return __awaiter(this, void 0, void 0, function* () {
            let id = {};
            let type = "all";
            let users = yield index_1.Models.getUsers(id, type);
            console.log(num);
            for (let i = 1; i < num + 1; i++) {
                let user = {
                    id: users.length + i,
                    name: "user",
                    cash: Math.floor(Math.random() * Math.floor(100000))
                };
                index_1.Models.writeData(user);
                console.log(user);
            }
            return "completed";
        });
    }
    static createManualUser(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let { id, name, cash } = data;
            let users = yield index_1.Models.getUsers(id, "all");
            let user = {
                id: users.length + 1,
                name: name,
                cash: cash
            };
            index_1.Models.writeData(user);
            return "completed";
        });
    }
    static move_cash(move, id, sum) {
        return __awaiter(this, void 0, void 0, function* () {
            let user_data = yield index_1.Models.getUsers(id, "search");
            if (move === "get") {
                user_data[0].cash = user_data[0].cash - sum;
            }
            else if (move === "put") {
                user_data[0].cash = user_data[0].cash + sum;
            }
            console.log(user_data);
            index_1.Models.reload_data(user_data);
            return [user_data, "completed"];
        });
    }
    static transferCash(id_1, id_2, sum) {
        return __awaiter(this, void 0, void 0, function* () {
            let user_1 = yield index_1.Models.getUsers(id_1, "search");
            let user_2 = yield index_1.Models.getUsers(id_2, "search");
            console.log(user_1, user_2);
            if (user_1.length > 0 && user_2.length > 0) {
                console.log("transfer");
                user_1[0].cash = user_1[0].cash - sum;
                user_2[0].cash = user_2[0].cash + sum;
                index_1.Models.reload_data(user_1);
                index_1.Models.reload_data(user_2);
            }
            else {
                console.log("error");
                return ("error");
            }
            return ["completed", user_1, user_2];
        });
    }
}
exports.User = User;
//# sourceMappingURL=user_move.js.map