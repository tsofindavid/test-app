"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const router = require("koa-router");
const index_1 = require("../models/index");
const user_move_1 = require("./user_move");
const route = new router();
route
    .get("/list", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    let id = {};
    let type = "all";
    let users = yield index_1.Models.getUsers(id, type);
    ctx.body = {
        status: "ok",
        users: users
    };
    console.log(users);
    yield next();
}))
    .post("/add_user", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    let { type, num } = ctx.body;
    if (type === "random") {
        ctx.body = yield user_move_1.User.createRandomUser(num);
    }
    else if (type === "manual") {
        ctx.body = yield user_move_1.User.createManualUser(ctx.body);
    }
    yield next();
}))
    .post("/move_cash", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    let { move, id, sum } = ctx.body;
    ctx.body = yield user_move_1.User.move_cash(move, id, sum);
    yield next();
}))
    .post("/transfer_cash", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("trans");
    let { id_1, id_2, sum } = ctx.body;
    ctx.body = yield user_move_1.User.transferCash(id_1, id_2, sum);
    yield next();
}));
route.post('/list', (ctx) => {
    console.log(ctx.body);
    console.log(ctx.request.body);
    console.log("xui");
    ctx.body = {
        status: 'ok'
    };
});
exports.default = route.middleware();
//# sourceMappingURL=index.js.map