"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa = require("koa");
const router = require("./routes");
const bodyParser = require("koa-bodyparser");
function app() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = new koa();
        app.use(bodyParser());
        app.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
            // the parsed body will store in ctx.request.body
            // if nothing was parsed, body will be an empty object {}
            ctx.body = ctx.request.body;
            yield next();
        }));
        app.use(router.default);
        return app;
    });
}
exports.default = app();
//# sourceMappingURL=app.js.map