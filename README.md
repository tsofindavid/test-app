for run program "npm run start"

http example 

for get user list :
    GET request,
    /user_list

for add user or users:
    1)method random:
        POST request,
        /add_user,
        body{
            "type":"random",
            "num": 2
        }
    2)method manual add:
        POST request,
            /add_user,
            body{
                "type":"manual",
                "name" : "man",
                "cash" : 100000
            }

for get user cash:
    POST request,
        /move_cash,
        body{
            "move":"get",
            "sum": 100000,
            "id": 1
        }

for put user cash:
    POST request,
        /move_cash,
        body{
            "move":"put",
            "sum": 100000,
            "id": 1
        }

for transfer money:
    POST request,
        /transfer_cash,
        body{
            "id_1":1,
            "id_2":3,
            "sum":"10"
        }

